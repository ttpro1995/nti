import CONST_ubuntu as CONST
from embedding_model import EmbeddingModel
from dataset import Dataset, Batch
def save_batch():
    EMPTY_path = None
    if (EMPTY_path == None):
        print 'Start creating embedding model with new EMPTY vector'
        print ('Embedding mode ', CONST.EMBEDDING_MODE)
        print ('Embedding path ', CONST.embedding_path)
        embedding_model = EmbeddingModel(CONST.embedding_path, CONST.EMBEDDING_MODE)
        print 'done creating embedding model'
    else:
        print 'Start creating embedding model'
        print 'EMPTY vector ',EMPTY_path
        print ('Embedding mode ', CONST.EMBEDDING_MODE)
        print ('Embedding path ', CONST.embedding_path)
        embedding_model = EmbeddingModel(CONST.embedding_path, CONST.EMBEDDING_MODE, EMPTY_path=EMPTY_path)
        print 'done creating embedding model'

    # create dataset
    print 'Start load dataset'
    dataset = Dataset()
    dataset.set_embedding_model(embedding_model)
    dataset.load_dataset(CONST.trees)
    dub_data = False
    print ('Dub data ', dub_data)
    print 'Done load dataset'

    x, y = dataset.create_embedding_dataset('train')
    x_dev, y_dev = dataset.create_embedding_dataset('dev')
    x_test, y_test = dataset.create_embedding_dataset('test')


    my_batch = Batch(x,y, dub_data=dub_data)
    my_batch.save_npy('saved_train')

    dev_batch = Batch(x_dev,y_dev, dub_data=dub_data)
    dev_batch.save_npy('saved_dev')

    test_batch = Batch(x_test, y_test, dub_data=dub_data)
    test_batch.save_npy('saved_test')

if __name__ == "__main__":
	save_batch()
