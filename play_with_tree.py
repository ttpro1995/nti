from models import NTIFullTreeMatching, NTIsentiment
import numpy as np

x = np.load('x.npy')
x_32 = np.load('x_32.npy')
x_32 = np.asarray(x_32, dtype='float32')

x_31 = np.load('x_31.npy')
x_31 = np.asarray(x_31, dtype='float32')
y = np.load('y.npy')

x = np.asarray(x, dtype='float32')

# model = NTIFullTreeMatching(n_units=300, gpu=-1)
# model.init_optimizer()

model_sent = NTIsentiment(n_units=300, gpu=-1)
model_sent.init_optimizer()

sent_data = [x_32, x_32, x_32, x_32]
y_data = np.asarray([0,1,1,1])
y_s, loss = model_sent.train(sent_data, y_data)
y_s = model_sent.predict(sent_data)
print (y_s)


print ('break')