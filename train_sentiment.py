from dataset import Dataset, Batch
from embedding_model import EmbeddingModel
import CONST_ubuntu as CONST
from models import NTIsentiment, NTIFullTreeMatchingSentiment
import time
import numpy as np
from sklearn.metrics import accuracy_score
from meowlogtool import log_util
from meowlogtool.log_util import StreamToLogger
import sys

# TODO: set y to 1 or 3

def load_small_dataset():
    model_sentiment = NTIsentiment.load('./10_epoch_meow', 300, -1)
    embedding_model = EmbeddingModel(CONST.w2v_pretrained)
    dataset = Dataset()
    dataset.set_embedding_model(embedding_model)
    dataset.load_dataset(CONST.trees)
    x, y = dataset.create_embedding_dataset('dev')
    my_batch = Batch(x, y)
    preds = []
    while (my_batch.has_batch()):
        t1 = time.time()
        x_batch, y_batch = my_batch.get_batch()

        pred = model_sentiment.predict(x_batch)
        preds.extend(pred)
        t2 = time.time()
        print "Batch time: %f milliseconds" % ((t2 - t1))

    score = accuracy_score(y, preds)
    print(score)

def continue_small_dataset():
    model_sentiment = NTIsentiment.load('./100_epoch_fix', 300, -1)
    model_sentiment.init_optimizer()
    embedding_model = EmbeddingModel(CONST.w2v_pretrained)
    dataset = Dataset()
    dataset.set_embedding_model(embedding_model)
    dataset.load_dataset(CONST.trees)
    x, y = dataset.create_embedding_dataset('dev')

    my_batch = Batch(x,y)

    for e in range(1, 75):
        my_batch.reset_batch()
        total_loss = 0
        n_batch = 0
        t1 = time.time()
        while(my_batch.has_batch()):
            x_batch, y_batch = my_batch.get_batch(batch_size=64)
            ys, loss = model_sentiment.train(x_batch, y_batch)
            total_loss+= loss.data
            n_batch+=1
        t2 = time.time()
        print ('epoch loss', total_loss/n_batch, 'in time ', (t2-t1))


    print 'save model'
    model_sentiment.save('100_epoch_fix')


def train_small_dataset():

    # create model
    model_sentiment = NTIsentiment(n_units=300, gpu=-1)
    model_sentiment.init_optimizer()
    print 'model'

    # create dataset
    embedding_model = EmbeddingModel(CONST.embedding_path, CONST.EMBEDDING_MODE)
    dataset = Dataset()
    dataset.set_embedding_model(embedding_model)
    dataset.load_dataset(CONST.trees)
    x, y = dataset.create_embedding_dataset('dev')
    print 'embedding'

    # print ('x ', len(x))
    # print ('y',  len(y))
    #
    # x_32 = np.load('x_32.npy')
    # x_32 = np.asarray(x_32, dtype='float32')


    # model = NTIFullTreeMatching(n_units=300, gpu=-1)
    # model.init_optimizer()

    # sent_data = [x_32, x_32, x_32, x_32]
    # y_data = np.asarray([1, 1, 1, 2])

    my_batch = Batch(x,y)

    for e in range(1, 10): # training
        my_batch.reset_batch()
        total_loss = 0
        n_batch = 0
        t1 = time.time()
        while(my_batch.has_batch()):
            x_batch, y_batch = my_batch.get_batch(batch_size=64)
            ys, loss = model_sentiment.train(x_batch, y_batch)
            total_loss+= loss.data
            n_batch+=1
        t2 = time.time()
        print ('epoch loss', total_loss/n_batch, 'in time ', (t2-t1))


    print 'save model'
    model_sentiment.save('10_epoch_meow')

def train_on_train_dataset(trained_model_path = None, EMPTY_path = None, ModelClass = NTIsentiment):
    print '------------START--------------------------------'

    # create model
    if (trained_model_path == None):
        print 'Create new model'
        model_sentiment = ModelClass(n_units=300, gpu=-1)
        model_sentiment.init_optimizer()
    else:
        print 'continues train model ', trained_model_path
        model_sentiment = ModelClass.load(trained_model_path, n_units=300, gpu=-1)
        model_sentiment.init_optimizer()
    print 'model'


    # create embedding
    if (EMPTY_path == None):
        print 'Start creating embedding model with new EMPTY vector'
        print ('Embedding mode ', CONST.EMBEDDING_MODE)
        print ('Embedding path ', CONST.embedding_path)
        embedding_model = EmbeddingModel(CONST.embedding_path, CONST.EMBEDDING_MODE)
        print 'done creating embedding model'
    else:
        print 'Start creating embedding model'
        print 'EMPTY vector ',EMPTY_path
        print ('Embedding mode ', CONST.EMBEDDING_MODE)
        print ('Embedding path ', CONST.embedding_path)
        embedding_model = EmbeddingModel(CONST.embedding_path, CONST.EMBEDDING_MODE, EMPTY_path=EMPTY_path)
        print 'done creating embedding model'

    # create dataset
    print 'Start load dataset'
    dataset = Dataset()
    dataset.set_embedding_model(embedding_model)
    dataset.load_dataset(CONST.trees)
    dub_data = True
    print ('Dub data ', dub_data)
    print 'Done load dataset'

    x, y = dataset.create_embedding_dataset('train')
    x_dev, y_dev = dataset.create_embedding_dataset('dev')
    x_test, y_test = dataset.create_embedding_dataset('test')


    my_batch = Batch(x,y, dub_data=dub_data)
    dev_batch = Batch(x_dev, y_dev, dub_data=dub_data)
    test_batch = Batch(x_test, y_test, dub_data=dub_data)

    print 'Start training'
    max_dev_score = 0
    early_stop_e = 50
    cur_early_stop = 0
    for e in range(0, 30): # go for 10 epoch
        my_batch.reset_batch(shuffle=True)
        dev_batch.reset_batch()
        test_batch.reset_batch()

        total_loss = 0
        n_batch = 0
        t1 = time.time()
        while(my_batch.has_batch()):
            x_batch, y_batch = my_batch.get_batch(batch_size=64)
            ys, loss = model_sentiment.train(x_batch, y_batch)
            total_loss+= loss.data
            n_batch+=1
        t2 = time.time()
        print ('epoch', e, ' loss', total_loss/n_batch, 'in time ', (t2-t1))

        # calculate dev score on dev set
        preds_dev = []
        while (dev_batch.has_batch()):
            x_dev_batch, y_dev_batch = dev_batch.get_batch()
            pred = model_sentiment.predict(x_dev_batch)
            preds_dev.extend(pred)
        dev_score = accuracy_score(y_dev, preds_dev)
        print ('epoch', e, ' dev_score', dev_score)

        if (dev_score > max_dev_score):
            max_dev_score = dev_score
            cur_early_stop = 0
        else:
            cur_early_stop +=1 # increase countdown when no improvement

        if (cur_early_stop >= early_stop_e):
            model_sentiment.save(str(e) + 'on_train_data_early_stop.save')
            print ('early stop on epoch',e)
            break

        if (e % 10 == 0):
            model_sentiment.save(str(e)+'on_train_data.save')

    print ('Done training')
    print ('Let test on test set')

    preds_test = []
    test_batch.reset_batch()
    while (test_batch.has_batch()):
        x_test_batch, y_test_batch = test_batch.get_batch()
        pred = model_sentiment.predict(x_test_batch)
        preds_test.extend(pred)
    test_score = accuracy_score(y_test, preds_test)
    print ('test_score ', test_score)
    model_sentiment.save('on_train_data_final.save')
    print ('------------------------------ALL DONE-----------------------------')

if __name__ == "__main__":
    # attach meow log tool
    logger = log_util.create_logger("NTI_sentiment", True, True, CONST.loggly_api, loggly_tag="NTIsentiment")
    logstream = StreamToLogger(logger)
    sys.stdout = logstream

    # train_small_dataset()
    # continue_small_dataset()
    # load_small_dataset()
    train_on_train_dataset(ModelClass=NTIFullTreeMatchingSentiment)
    # load_small_dataset()
