import math
import sys
import time
import copy
import numpy as np
import six
from math import log
from chainer import cuda, Variable, FunctionSet, optimizers
import chainer.functions  as F
import chainer.links as L
import chainer

class NTIsentiment(chainer.Chain):

    """docstring for NTIsentiment"""
    def __init__(self, n_units, gpu):
        super(NTIsentiment, self).__init__(
            h_lstm = L.LSTM(n_units, n_units), # use
            m_lstm = L.LSTM(n_units, n_units), # after attend
            h_x = F.Linear(n_units, 4*n_units), #
            h_h = F.Linear(n_units, 4*n_units), #
            w_ap = F.Linear(n_units, n_units), # attend
            w_we = F.Linear(n_units, 1), # no where to be found
            w_c = F.Linear(n_units, n_units), # attend
            w_q = F.Linear(n_units, n_units), # attend
            h_l1 = F.Linear(n_units, 1024), # 2*n_units because concat 2 word (need modify for sentiment)
            l_y = F.Linear(1024, 2),

            # 2 layer MLP
            l1 = F.Linear(300, 200),
            l2 = F.Linear(200, 200),
            l3 = F.Linear(200,2) # 2 class (good, bad)
        )  # get final output


        self.__n_units = n_units
        self.__gpu = gpu
        self.__mod = cuda.cupy if gpu >= 0 else np
        for param in self.params():
            data = param.data
            data[:] = np.random.uniform(-0.1, 0.1, data.shape)
        if gpu >= 0:
            cuda.get_device(gpu).use()
            self.to_gpu()

    def init_optimizer(self):
        initial_learning_rate = 0.001
        weightdecay = 0.00003
        self.__opt = optimizers.Adam(alpha=initial_learning_rate, # initial learning rate
                                     beta1=0.9, beta2=0.999, eps=1e-08)
        self.__opt.setup(self)
        self.__opt.add_hook(chainer.optimizer.GradientClipping(40))

        self.__opt.add_hook(chainer.optimizer.WeightDecay(weightdecay)) # 0.00003 # l2 regular
        print ('-- hyperparam --' )
        print ('initial learning rate ',initial_learning_rate)
        print ('weight decay ', weightdecay)


    def save(self, filename):
        chainer.serializers.save_npz(filename, self)

    @staticmethod
    def load(filename, n_units, gpu):
        self = NTIsentiment(n_units, gpu)
        chainer.serializers.load_npz(filename, self)
        return self

    def reset_state(self):
        self.h_lstm.reset_state()
        self.m_lstm.reset_state()

    def __forward(self, train, x_batch, y_batch = None):
        model = self
        n_units = self.__n_units
        mod = self.__mod
        gpu = self.__gpu
        batch_size = len(x_batch)
        x_len = len(x_batch[0])
        depth = int(log(x_len, 2))  + 1

        self.reset_state()

        list_a = [[] for i in range(2**depth-1)]
        list_c = [[] for i in range(2**depth-1)]
        zeros = mod.zeros((batch_size, n_units), dtype=np.float32)
        for l in xrange(x_len):
            x_data = mod.array([x_batch[k][l] for k in range(batch_size)]) # get each corresponding word for each sentence in batch
            x_data = Variable(x_data, volatile=not train)
            x_data = model.h_lstm(F.dropout(x_data, ratio=0.2, train=train))
            list_a[x_len-1+l] = x_data
            list_c[x_len-1+l] = model.h_lstm.c #Variable(zeros, volatile=not train)

        for d in reversed(range(1, depth)):
            for s in range(2**d-1, 2**(d+1)-1, 2):
                l = model.h_x(F.dropout(list_a[s], ratio=0.2, train=train))
                r = model.h_h(F.dropout(list_a[s+1], ratio=0.2, train=train))
                c_l = list_c[s]
                c_r = list_c[s+1]
                c, h = F.slstm(c_l, c_r, l, r)
                list_a[(s-1)/2] = h
                list_c[(s-1)/2] = c

        # get h from root
        # s, s+1 : child
        # (s-1)/2 : parent
        h_root = list_a[0] # (batch_size, 300)

        model.m_lstm.reset_state()


        #hs = F.relu(model.h_l1(h_root)) # shape(batch_size,1024)
        #y = model.l_y(F.dropout(hs, ratio=0.2, train=train))

        # apply MLP to sentence representations
        # h_root is sentence representations
        h1 = F.tanh(model.l1(F.dropout(h_root, ratio=0.2, train=train)))
        h2 = F.tanh(model.l2(F.dropout(h1, ratio=0.2, train=train)))
        y = F.softmax(model.l3(F.dropout(h2, ratio=0.2, train=train)))

        preds = mod.argmax(y.data, 1).tolist()

        accum_loss = 0 if train else None
        if train:
            if gpu >= 0:
                y_batch = cuda.to_gpu(y_batch)
            lbl = Variable(y_batch, volatile=not train)
            accum_loss = F.softmax_cross_entropy(y, lbl)

        return preds, accum_loss

    def train(self, x_batch, y_batch):
        self.__opt.zero_grads()
        preds, accum_loss = self.__forward(True, x_batch, y_batch=y_batch)
        accum_loss.backward()
        self.__opt.update()
        return preds, accum_loss

    def predict(self, x_batch):
        return self.__forward(False, x_batch)[0]
