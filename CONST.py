
# put word2vec pretrain path here
w2v_pretrained = "D:\APCS\_Research\data\GoogleNews-vectors-negative300.bin"
glove_pretrained = "/media/vdvinh/25A1FEDE380BDADA/thien_code/treelstm/data/glove/glove.840B.300d.txt"

# put sentiment treebank dataset here
trees = ".\trees"

# loggly api key
loggly_api = "07510d18-73c8-4552-8227-264c111ab3ab"

# config
EMBEDDING_MODE_GLOVE = "glove"
EMBEDDING_MODE_W2V = "word2vec"
embedding_path = ""
EMBEDDING_MODE = EMBEDDING_MODE_W2V

if EMBEDDING_MODE == EMBEDDING_MODE_GLOVE:
    embedding_path = glove_pretrained
elif EMBEDDING_MODE == EMBEDDING_MODE_W2V:
    embedding_path = w2v_pretrained

