import tree_util
import pytreebank
from embedding_model import EmbeddingModel
import sys
import numpy as np
import time


w2v_pretrained = "D:\APCS\_Research\data\GoogleNews-vectors-negative300.bin"

def create_sample_embedding(sentence):
    embedding_model = EmbeddingModel(w2v_pretrained)
    sentence_vec = []
    for word in sentence.split():
        vec = embedding_model.embedding(word)  # some word will be ignore because no embedding
        sentence_vec.append(vec)
    sentence_vec = np.asarray(sentence_vec)
    return sentence_vec


def create_embbeded_dataset_from_tree(trees):
    """

    :param trees: trees
    :return: x, labels
    """


def create_embedded_dataset():
    trees = pytreebank.load_sst('trees')
    # trees_train = trees["train"]
    trees_dev = trees["dev"]
    # trees_test = trees["test"]

    sentences, labels = tree_util.load_dataset(trees_dev)
    embedding_model = EmbeddingModel(w2v_pretrained)

    x = []

    for i in range(0,len(labels)):
        sentence = sentences[i]
        # embedding
        sentence_vec = []
        for word in sentence.split():
            vec = embedding_model.embedding(word)  # some word will be ignore because no embedding
            sentence_vec.append(vec)
        sentence_vec = np.asarray(sentence_vec)
        x.append(sentence_vec)

    return (x, labels)